#!/bin/bash

set -e

if [[ -z $1 || -z $2 ]]; then
    echo "compile-run.sh <day number> <day letter> [option arguments to pass to app]"
    exit 1;
fi

objfw-compile Day$1/Day$1$2.m -o Day$1/Day$1$2.app

Day$1/Day$1$2.app $3
