#import "Day02B.h"

OF_APPLICATION_DELEGATE(Day02B)

@implementation Day02B
- (void)applicationDidFinishLaunching
{
    printf("Running Day02B check...\n");

    OFArray *arguments = [OFApplication arguments];

    if ([arguments count] == 0 || [arguments count] > 1)
    {
        printf("Usage: Day02B.app <directions file>\n");
        [OFApplication terminate];
    }

    OFString *filePath = [arguments objectAtIndex: 0];

    printf("Loading file...\n");

    if (![[OFFileManager defaultManager] fileExistsAtPath: filePath])
    {
        printf("Directions file not found");
        [OFApplication terminate];
    }

    OFFile *directionsFiles = [[OFFile alloc] initWithPath: filePath mode: @"r"];
    OFMutableArray *directions = [[OFMutableArray alloc] initWithCapacity: 100];
    OFString *line;
    do
    {
        line = [directionsFiles readLine];
        if (line != nil)
        {
            [directions addObject: line];
        }
    } while (line != nil);

    int position = 0;
    int depth = 0;
    int aim = 0;

    int movement = 0;

    for (int i = 0; i < [directions count]; i++)
    {
        printf("Direction: %s\n", [[directions objectAtIndex: i] cStringWithEncoding:OFStringEncodingUTF8]);

        if ([[directions objectAtIndex: i] containsString: @"forward"])
        {
            movement = [self parseNumber: [directions objectAtIndex: i]];

            position += movement;
            if (aim != 0 )
            {
                depth += aim * movement;
            }

            printf("Moving forward; new position: %d, new depth: %d\n", position, depth);
        }
        else if ([[directions objectAtIndex: i] containsString: @"up"])
        {
            aim -= [self parseNumber: [directions objectAtIndex: i]];
            printf("Pointing aim up, new aim: %d\n", aim);
        }
        else if ([[directions objectAtIndex: i] containsString: @"down"])
        {
            aim += [self parseNumber: [directions objectAtIndex: i]];
            printf("Pointing aim down, new aim: %d\n", aim);
        }
        else
        {
            printf("Error reading directions!\n");
        }
    }

    printf("Final Position: %d\nFinal Depth: %d\nTotal multiplied: %d", position, depth, position * depth);

    [OFApplication terminate];
}

- (int) parseNumber: (OFString *)input
{
    int location = [input indexOfCharacterFromSet: [OFCharacterSet characterSetWithCharactersInString: @"0123456789"]];

    if (location == OFNotFound)
    {
        printf("Couldn't find number!\n");
        return 0;
    }
    return((int)[[input substringFromIndex: location] unsignedLongLongValueWithBase: 10]);
}

@end
