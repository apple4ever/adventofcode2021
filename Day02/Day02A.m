#import "Day02A.h"

OF_APPLICATION_DELEGATE(Day02A)

@implementation Day02A
- (void)applicationDidFinishLaunching
{
    printf("Running Day02A check...\n");

    OFArray *arguments = [OFApplication arguments];

    if ([arguments count] == 0 || [arguments count] > 1)
    {
        printf("Usage: Day02A.app <directions file>\n");
        [OFApplication terminate];
    }

    OFString *filePath = [arguments objectAtIndex: 0];

    printf("Loading file...\n");

    if (![[OFFileManager defaultManager] fileExistsAtPath: filePath])
    {
        printf("Directions file not found");
        [OFApplication terminate];
    }

    OFFile *directionsFiles = [[OFFile alloc] initWithPath: filePath mode: @"r"];
    OFMutableArray *directions = [[OFMutableArray alloc] initWithCapacity: 100];
    OFString *line;
    do
    {
        line = [directionsFiles readLine];
        if (line != nil)
        {
            [directions addObject: line];
        }
    } while (line != nil);

    int position = 0;
    int depth = 0;

    for (int i = 0; i < [directions count]; i++)
    {
        printf("Direction: %s\n", [[directions objectAtIndex: i] cStringWithEncoding:OFStringEncodingUTF8]);
        if ([[directions objectAtIndex: i] containsString: @"forward"])
        {
            position += [self parseNumber: [directions objectAtIndex: i]];
            printf("Moving forward, new position: %d\n", position);
        }
        else if ([[directions objectAtIndex: i] containsString: @"up"])
        {
            depth -= [self parseNumber: [directions objectAtIndex: i]];
            printf("Moving up, new depth: %d\n", depth);
        }
        else if ([[directions objectAtIndex: i] containsString: @"down"])
        {
            depth += [self parseNumber: [directions objectAtIndex: i]];
            printf("Moving down, new depth: %d\n", depth);
        }
        else
        {
            printf("Error reading directions!\n");
        }
    }

    printf("Final Position: %d\nFinal Depth: %d\nTotal multiplied: %d", position, depth, position * depth);

    [OFApplication terminate];
}

- (int) parseNumber: (OFString *)input
{
    int location = [input indexOfCharacterFromSet: [OFCharacterSet characterSetWithCharactersInString: @"0123456789"]];

    if (location == OFNotFound)
    {
        printf("Couldn't find number!\n");
        return 0;
    }
    return((int)[[input substringFromIndex: location] unsignedLongLongValueWithBase: 10]);
}

@end
