#import "Day01A.h"

OF_APPLICATION_DELEGATE(Day01A)

@implementation Day01A
- (void)applicationDidFinishLaunching
{
    printf("Running Day01A check...\n");

    OFArray *arguments = [OFApplication arguments];

    if ([arguments count] == 0 || [arguments count] > 1)
    {
        printf("Usage: Day01A.app <measurements file>\n");
        [OFApplication terminate];
    }

    OFString *filePath = [arguments objectAtIndex: 0];

    printf("Loading file...\n");

    if (![[OFFileManager defaultManager] fileExistsAtPath: filePath])
    {
        printf("Measurements file not found");
        [OFApplication terminate];
    }

    OFFile *measurementsFiles = [[OFFile alloc] initWithPath: filePath mode: @"r"];
    OFMutableArray *measurements = [[OFMutableArray alloc] initWithCapacity: 100];
    OFString *line;
    do
    {
        line = [measurementsFiles readLine];
        if (line != nil)
        {
            [measurements addObject: [OFNumber numberWithInt: (int)[line unsignedLongLongValueWithBase: 0]]];
        }
    } while (line != nil);

    int increasing = 0;
    int decreasing = 0;

    for (int i = 0; i < [measurements count]; i++)
    {
        printf(" %d", [[measurements objectAtIndex: i] intValue]);

        if (i > 0)
        {
            if ( [[measurements objectAtIndex: i] intValue] > [[measurements objectAtIndex: i-1] intValue])
            {
                printf(" - Increasing!\n");
                increasing++;
            }
            else
            {
                printf(" - Decreasing!\n");
                decreasing++;
            }
        }
    }

    printf("Total count increasing: %d\n", increasing);

    [OFApplication terminate];
}
@end
