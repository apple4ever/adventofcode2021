#import "Day01B.h"

OF_APPLICATION_DELEGATE(Day01B)

@implementation Day01B
- (void)applicationDidFinishLaunching
{
    printf("Running Day01B check...\n");

    OFArray *arguments = [OFApplication arguments];

    if ([arguments count] == 0 || [arguments count] > 1)
    {
        printf("Usage: Day01B.app <measurements file>\n");
        [OFApplication terminate];
    }

    OFString *filePath = [arguments objectAtIndex: 0];

    printf("Loading file...\n");

    if (![[OFFileManager defaultManager] fileExistsAtPath: filePath])
    {
        printf("Measurements file not found");
        [OFApplication terminate];
    }

    OFFile *measurementsFiles = [[OFFile alloc] initWithPath: filePath mode: @"r"];
    OFMutableArray *measurements = [[OFMutableArray alloc] initWithCapacity: 100];
    OFString *line;
    do
    {
        line = [measurementsFiles readLine];
        if (line != nil)
        {
            [measurements addObject: [OFNumber numberWithInt: (int)[line unsignedLongLongValueWithBase: 0]]];
        }
    } while (line != nil);

    int increasing = 0;
    int decreasing = 0;

    for (int i = 0; i < [measurements count]; i++)
    {
        if (i > 2)
        {
            int firstSet = 0;
            int secondSet = 0;

            for (int j = 0; j < 3; j++)
            {
                firstSet += [[measurements objectAtIndex: i-j-1] intValue];
                secondSet += [[measurements objectAtIndex: i-j] intValue];
            }

            printf("Set %d - %d", i-1, firstSet);
            printf(" - %d", secondSet);

            if ( secondSet > firstSet)
            {
                printf(" - Increasing!\n");
                increasing++;
            }
            else
            {
                printf(" - Decreasing!\n");
                decreasing++;
            }
        }
    }

    printf("Total count increasing: %d\n", increasing);

    [OFApplication terminate];
}
@end
